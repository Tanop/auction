<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Tax;

class CreateTaxTable extends Migration
{   
   /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('min_value')->nullable()->unique();
            $table->integer('max_value')->nullable()->unique();
            $table->integer('percent')->nullable();
            $table->timestamps();
        });

        Tax::create([
            'min_value' => 1,
            'max_value' => 100,
            'percent' => 3,
        ]);

        Tax::create([
            'min_value' => 100,
            'max_value' => 1000,
            'percent' => 5,
        ]);

        Tax::create([
            'min_value' => 1000,
            'max_value' => 10000,
            'percent' => 7,
        ]);

        Tax::create([
            'min_value' => 10000,
            'max_value' => 100000,
            'percent' => 11,
        ]);

        Tax::create([
            'min_value' => 100000,
            'max_value' => 10000000,
            'percent' => 20,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax');
    }
}