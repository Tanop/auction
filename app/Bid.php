<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'auction_id', 'value'];

    public function auction()
    {
    	return $this->belongsTo(Auction::class);
    }

    public function user() {
    	return $this->belongsTo(User::class);
    }
}
