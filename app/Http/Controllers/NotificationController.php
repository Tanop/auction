<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Notification;

class NotificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $notifications = $request->user()->notifications->sortByDesc('created_at');

        foreach ($notifications as $notification) {
            $notification->seen = true;
            $notification->save();
        }

        return view('notification.index', ['notifications' => $notifications]);
    }

    public function show(Request $request, $notification_id)
    {
        $notification = Notification::find($notification_id);
        return response()->json($notification);
    }

    public function store(Request $request)
    {
        $notification = Notification::create([
            'message' => $request->message, 
            'user_id' => $request->user_id,
        ]);

        return response()->json($notification);
    }

    public function destroy(Request $request, $notification_id)
    {
        Notification::find($notification_id)->delete();
        return response()->json([
            'Success' => 'OK',
        ]);
    }
}
