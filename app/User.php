<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Invitation;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'balance', 'first_name', 'last_name', 'mid_name', 'gender', 'age', 'admin', 'vip', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get all of the products for the user.
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    /**
     * One user belongs to many auctions
     */
    public function auctions() 
    {
        return $this->belongsToMany(Auction::class)->withTimestamps();
    }

    /**
     * Get all bid that belong to this user.
     */
    public function bids() 
    {
        return $this->hasMany(Bid::class);
    }

    /**
     * Get all message that user has
     */
    public function messages()
    {
        return Message::where('user_id', $this->id)->orWhere('sender', $this->id)->orderBy('created_at', 'desc')->get();
    }

    /**
     * Get all notification that user has
     */
    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }


    /**
     * Get all message that user hasn't seen
     */
    public function messages_havent_seen() 
    {
        return $this->hasMany(Message::class)->where('seen', false)->get();
    }

    /**
     * Get all notification that user hasn't seen
     */
    public function notifications_havent_seen() 
    {
        return $this->hasMany(Notification::class)->where('seen', false)->get();
    }

     /**
     * Get all notiinvitationsfication that user hasn't seen
     */
    public function invitations_havent_seen() {
        return Invitation::where([
            ['to', $this->id],
            ['solved', 0],
        ])->get();
    }
}
