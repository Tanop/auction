<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'price', 'max_price'];

    public function isInAuction() 
    {
        return ($this->auction !== null && $this->auction->isRunning());
    }

    /**
     * Get the user that owns the product.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get auction record associated with the product.
     */
    public function auction() 
    {
        return $this->hasOne(Auction::class);
    }

    /**
     * Get images of product
     */
    public function photos() {
        return $this->hasMany(Photo::class);
    }
}
