<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'message'];

    /**
     * Return reciever of this notification
     */
    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
