<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['filename', 'product_id'];

	/**
	 * Return product that photo belongs to
	 * @return Product
	 */
    public function product()
    {
    	return $this->belongsTo(Product::class);
    }
}
