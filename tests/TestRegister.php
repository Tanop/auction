<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TestRegister extends TestCase
{
    public function testNullForm()
    {
        $this->visit('/register')
             ->type('', 'username')
             ->type('', 'email')
             ->type('', 'password')
             ->type('', 'password_confirmation')             
             ->type('', 'first_name')
             ->type('', 'last_name')
             ->type('', 'mid_name')
             ->type('', 'age')
             ->select('F', 'gender')
             ->press('Зарегистрироваться')
             ->seePageIs('/register');
    }

    public function testFirstName()
    {
        $this->visit('/register')
             ->type('new_user', 'username')
             ->type('user@gmail.com', 'email')
             ->type('user_pass', 'password')
             ->type('user_pass', 'password_confirmation')             
             ->type('', 'first_name')
             ->type('Mike', 'last_name')
             ->type('Doe', 'mid_name')
             ->type('24', 'age')
             ->select('M', 'gender')
             ->press('Зарегистрироваться')
             ->seePageIs('/register');
    }

    public function testEmail()
    {
        $this->visit('/register')
             ->type('new_user', 'username')
             ->type('user@', 'email')
             ->type('user_pass', 'password')
             ->type('user_pass', 'password_confirmation')
             ->type('Alex', 'first_name')
             ->type('Mike', 'last_name')
             ->type('Doe', 'mid_name')
             ->type('24', 'age')
             ->select('M', 'gender')
             ->press('Зарегистрироваться')
             ->seePageIs('/register');
    }

    public function testPassword()
    {
        $this->visit('/register')
             ->type('new_user', 'username')
             ->type('user@gmail.com', 'email')
             ->type('pas', 'password')
             ->type('pas', 'password_confirmation')
             ->type('Alex', 'first_name')
             ->type('Mike', 'last_name')
             ->type('Doe', 'mid_name')
             ->type('24', 'age')
             ->select('M', 'gender')
             ->press('Зарегистрироваться')
             ->seePageIs('/register');
    }

    public function testAgeMin()
    {
        $this->visit('/register')
             ->type('new_user', 'username')
             ->type('user@gmail.com', 'email')
             ->type('user_pass', 'password')
             ->type('user_pass', 'password_confirmation')
             ->type('Alex', 'first_name')
             ->type('Mike', 'last_name')
             ->type('Doe', 'mid_name')
             ->type('3', 'age')
             ->select('M', 'gender')
             ->press('Зарегистрироваться')
             ->seePageIs('/register');
    }

    public function testAgeMax()
    {
        $this->visit('/register')
             ->type('new_user', 'username')
             ->type('user@gmail.com', 'email')
             ->type('user_pass', 'password')
             ->type('user_pass', 'password_confirmation')
             ->type('Alex', 'first_name')
             ->type('Mike', 'last_name')
             ->type('Doe', 'mid_name')
             ->type('100', 'age')
             ->select('M', 'gender')
             ->press('Зарегистрироваться')
             ->seePageIs('/register');
    }

    public function testLoginIsExist()
    {
        $this->visit('/register')
             ->type('admin', 'username')
             ->type('user@gmail.com', 'email')
             ->type('user_pass', 'password')
             ->type('user_pass', 'password_confirmation')
             ->type('Alex', 'first_name')
             ->type('Mike', 'last_name')
             ->type('Doe', 'mid_name')
             ->type('24', 'age')
             ->select('M', 'gender')
             ->press('Зарегистрироваться')
             ->seePageIs('/register');
    }

    public function testCorrectRun()
    {
        $this->visit('/register')
             ->type('test_user', 'username')
             ->type('new@gmail.com', 'email')
             ->type('secret', 'password')
             ->type('secret', 'password_confirmation')
             ->type('Alex', 'first_name')
             ->type('Mike', 'last_name')
             ->type('Doe', 'mid_name')
             ->type('24', 'age')
             ->select('M', 'gender')
             ->press('Зарегистрироваться')
             ->seePageIs('/home');
    }
}
