var url = "/notification";


$(".delete").on( "click", function(e) {
	// setup
	$.ajaxSetup({
		headers: {
	    	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});

	var notification_id = $(this).val();

	// delete
	$.ajax({
		type: "DELETE",
		url: url + "/" + notification_id,
		success: function(data) {			
			console.log(data);
			$("#ms-row-" + notification_id).remove();
		}
	});

	e.preventDefault();
});