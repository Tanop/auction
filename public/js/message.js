var url = "/message";

function send_to_admin() {
	$("#modal-reply").modal('show');
	$("#reciever_username").text("admin");
	$("input[name=user_id]").val(1);
}

$(".delete").on( "click", function(e) {
	// setup
	$.ajaxSetup({
		headers: {
	    	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});

	var message_id = $(this).val();

	// delete
	$.ajax({
		type: "DELETE",
		url: url + "/" + message_id,
		success: function(data) {			
			console.log(data);
			$("#ms-row-" + message_id).remove();
		}
	});

	e.preventDefault();
});

/**
 * Show dialog to reply
 */
$(".reply").on( "click", function(e) {
	// setup
	$.ajaxSetup({
		headers: {
	    	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});

	var message_id = $(this).val();

	$.ajax({
		type: "GET",
		url: url + "/" + message_id,
		success: function(data) {
			$("#modal-reply #reciever_username").text(data.sender);
			$("input[name=user_id]").val(data.body.sender);
			$("input[name=sender]").val(data.body.user_id);
		}
	});

	e.preventDefault();
});

/**
 * Send message
 */
$('#mess-form').submit(function(e) {
	// setup ajax
	$.ajaxSetup({
    	headers: {
        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});

	var formData = {
		user_id : Number($("input[name=user_id]").val()),
		message : $("#message").val(),
		sender : Number($("input[name=sender]").val()),
	};

	$.ajax({
		type: "POST",
		url: url,
		data: formData,
		success: function(data) {
			console.log(data);
			alert("Отправлено");
			$("textarea[name=message]").val('');
			$("#modal-reply").modal('hide');
		}
	});

	e.preventDefault();
});
