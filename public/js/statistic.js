$('#btn-do-statistic').on("click", function(event) {
    //alert("Submited");

    // setup
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var formData = {
        from : $('input[name="start_time"]').val(),
        to : $('input[name="end_time"]').val(),
        num_deals : $('#check-deals').is(":checked"),
        total_deals_value : $('#check-total-deals-value').is(":checked"),
        top_active_users : $('#check-top-active-user').is(":checked"),
        n_users : $('#n-users').val(),
        top_deal : $('#check-top-deals').is(":checked"),        
        n_deals : $('#n-deals').val(),
    };

    // POST
    $.ajax({
        type: "POST",
        url: "/statistic/",
        data: formData, 
        dataType: 'json',
        success: function(data) {			
            console.log(data);
        }
    });
    
    //console.log(formData);

    event.preventDefault();
});