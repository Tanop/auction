$('.btn-delete-user').on("click", function() {
	// get user id
	var user_id = $(this).val();
	// ajax get
	$.get("/user/" + user_id, function(data) {
		console.log(data);
		// show user name in modal
		$('#username').text(data.username);
	});
	// transfer userid to hidden input in modal
	$('#delete_user_id').val(user_id);
	// show modal
	$('#confirm-modal').modal("show");
});

$('#change-vip-button').on("click", function(e) {
	var new_value = Number($('input[name="vip_limit_value"]').val());
	// Validate here ....

	// setup ajax
	$.ajaxSetup({
    	headers: {
        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});

	// ajax post
	$.ajax({
		type: "POST",
		url: "/system/vip-limit",
		data: {
			value: new_value,
		}, 
		success: function(data) {		
			console.log(data);
		}
	});

	//console.log(value);
	e.preventDefault();
});

$('.btn-active-user').on("click", function() {
	// get user id
	var user_name = $(this).attr('username');
	var user_id = $(this).val();
	var button = $(this);
	console.log(user_id);
	console.log(user_name);
	
	// setup ajax
	$.ajaxSetup({
    	headers: {
        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});

	// ajax post
	$.ajax({
		type: "POST",
		url: "/user/enable/" + user_name,
		data: {
			username : user_name,
		}, 
		success: function(data) {		
			console.log(data);

			button.hide();
			$('#user-row-' + user_id).removeClass('danger');
			$('#user-row-' + user_id).addClass('info');
		}
	});
	
});

$('#delete-confirmed').on("click", function() {
	// setup ajax
	$.ajaxSetup({
    	headers: {
        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});
	// get user id
	var user_id = $('#delete_user_id').val();
	$.ajax({
		type: "DELETE",
		url: "/user/" + user_id,
		success: function(data) {
			// logging
			console.log("Deleted");
			// remove row that show deleted user
			$('#user-row-' + user_id).remove();
			// hide modal
			$('#confirm-modal').modal("hide");
		}
	});

}); 

$('.btn-edit-balance').on( "click", function (e){
	// get user id stored in attr value of button
	var user_id = $(this).val();

	// ajax get
	$.get("/user/" + user_id, function(data) {
		console.log(data);
		// show balance in input
		$('#balance-input').val(data.balance);
		$('#user_id').val(data.id);
	});
	
	// show modal
	$('#balance-modal').modal("show");
});

$('#balance-change').on( "click", function(e) {
	// setup ajax
	$.ajaxSetup({
    	headers: {
        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});

	// get user id
    var user_id = $('#user_id').val();

	// ajax put
	$.ajax({
		type: "PUT",
		url: "/balance/" + user_id,
		data: {
			balance: $('#balance-input').val()
		}, 
		success: function(data) {			
			console.log("Balance of ", data.username, " to ", data.balance);
			$('#balance-cell-' + user_id).text(data.balance);
			$('#balance-modal').modal("hide");	// hide modal
		}
	});
});

$("#change-tax-button").on( "click", function(e) {
	$(".tax-input").each(function() {
		var tax_value = Number($(this).val());
		var tax_id = $(this).attr("id");

		if (tax_value != 0) {
			$.ajaxSetup({
		    	headers: {
		        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});	

			$.ajax({
				type: "PUT",
				url:  "/tax/" + tax_id,
				data: {
					value: tax_value
				}, 
				success: function(data) {			
					console.log("Tax with id", data.id, "is set to ", data.percent);
					console.log($("input[name=tax-" + tax_id + "]"));
					$("input[name=tax-" + tax_id + "]").attr('placeholder', tax_value);
					$("input[name=tax-" + tax_id + "]").val('');
				}
			});	
		} 
	});

	e.preventDefault();
});