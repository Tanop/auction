@extends('layouts.app')

@section('content')
<!-- Table of all messages, which is sent to this user -->
<div class="panel panel-default">
    <div class="panel-heading">
        <h4>Ваши сообщения</h4>
    </div>    

    <div class="panel-body">
        <table class="table">
        	<thead>
        		<th>Отправитель</th>
        		<th>Сообщение</th>
        		<th width="25%"></th>
        	</thead>
        	<tbody id="message-table">
        		@foreach ($messages as $message)
        		<tr id="ms-row-{{ $message->id }}">
        			<td>{{ App\User::find($message->sender)->username }}</td>
        			<td>{{ $message->message }}</td>
        			<td>
        				<button data-toggle="modal" data-target="#modal-reply" class="reply btn btn-success" value="{{ $message->id }}">Ответить</button>
        				<button class="delete btn btn-danger" value="{{ $message->id }}">Удалить</button>
        			</td>
        		</tr>
        		@endforeach
        	</tbody>
        </table>
    </div>
</div>
<!-- End of table -->

<!-- Link to back page -->
<div class="panel panel-default">
    <div class="panel-body">
        <a href="/product" class="btn btn-primary">В личный кабинет</a>
        @if (!Auth::user()->admin)
        <button onclick="send_to_admin();" class="btn btn-warning">Запросить деньги</button>
        @endif
    </div>
</div>
<!-- End of link to back page -->

<!-- Modal for reply message -->
<div role="dialog" class="modal fade" id="modal-reply">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4>Новое сообщение к <span id="reciever_username"></span></h4>
            </div>
            <div class="modal-body">
                <form id="mess-form">       
                	<input type="hidden" name="user_id">
                    <input type="hidden" name="sender" value="{{ Auth::user()->id }}">          
                    <div class="form-group">
                        <textarea class="form-control" name="message" id="message" rows="5" placeholder="Введите сообщение..."></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                
                <button type="submit" form="mess-form" class="btn btn-primary">Отправить</button>
            </div>
        </div>
    </div>
</div>
<!-- End of modal for reply message -->
@endsection

@push('scripts')
	<script src="/js/message.js"></script>
@endpush