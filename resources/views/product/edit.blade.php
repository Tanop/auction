@extends('product.root')

@section('request_balance', '')

@section('form-title', 'Изменение товара')

@section('form-action')
	/product/{{ $product->id }}
@endsection

@section('name-value')
	value="{{ $product->name }}"
@endsection

@section('description-value')
{{ $product->description }}
@endsection

@section('price-value')
	@if ($product->auction !== null) value="{{ $product->auction->price }}" @endif
@endsection

@section('max-price-value')
 	@if ($product->auction !== null) value="{{ $product->auction->max_price }}" @endif
@endsection

@section('btn-value', 'Изменить')

@section('check-auction')
	@if ($product->auction !== null) checked @endif
@endsection

@section('check-private')
	@if (!$product->auction->public) checked @endif
@endsection

@section('start-time-value')
	@if ($product->auction !== null) value="{{ date('Y-m-d\TH:i', $product->auction->start_at) }}" @endif
@endsection

@section('end-time-value')
	@if ($product->auction !== null) value="{{ date('Y-m-d\TH:i', $product->auction->end_at) }}" @endif
@endsection

@section('photos')
	<div class="col-md-6 col-md-offset-4">
		@foreach ($product->photos as $photo)
			<img src="/uploads/product_images/{{ $photo->filename }}" class="img-thumbnail" style="max-width: 75px; height: auto;">
		@endforeach
	</div>
@endsection