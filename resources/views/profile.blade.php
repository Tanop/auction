@extends('layouts.app')

@section('content')
<!-- Display Current profile -->
<div class="panel panel-default">
    <div class="panel-heading">Информация о пользователе</div>
    
    <div class="panel-body">

        <div class="row">
            <div class="col-md-3 col-md-offset-3">
                <img src="/uploads/avatars/{{ $user->avatar }}" alt="" class="img-circle" style="height: 150px; width: 150px;"> 
            </div>

            <div class="col-md-1">
                <p><strong>Логин</strong></p>
                <p><strong>ФИО</strong></p>
                <p><strong>Возраст</strong></p>
                <p><strong>Пол</strong></p>
                <p><strong>Текущий счет</strong></p>
            </div>

            <div class="col-md-3">
                <p>{{ $user->username }}</p>
                <p>{{ $user->last_name }} {{ $user->first_name }} {{ $user->mid_name }} </p>
                <p>{{ $user->age }}</p>
                <p>
                    @if ( $user->gender === "F")
                        Жен.
                    @else
                        Муж.
                    @endif
                </p>
                <p>
                    {{ $user->balance }} монет
                </p>
            </div>
        </div>

    </div>
</div>

<!-- Form to change profile -->
<div class="separated">
    <div class="panel-heading"><h3>Редактировать информацию</h3></div>

    <div class="panel-body">
        <!-- Form begin -->
        <form class="form-horizontal" enctype="multipart/form-data" action="/profile" method="POST">
            {{ csrf_field() }}

            <!-- Avatar -->
            <div class="form-group{{ $errors->has('avatar') ? ' has-error' : '' }}">
                <div class ="row">
                    <label for="avatar" class="control-label col-sm-2" ><h4>Аватар</h4></label>
                    <div class="col-sm-10">
                        <input type="file" name="avatar">
                        @if ($errors->has('avatar'))
                            <span class="help-block">
                                <strong><h4>{{ $errors->first('avatar') }}</h4></strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
            
            <!-- First name -->

                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                    <div class = "row">
                    <div class="col-md-3" style="text-align: right">
                        <label for="last_name" class="control-label col-sm-2"><h4>Фамилия</h4></label>
                    </div>
                    <div class="col-md-9">
                        <input class="form-control" type="text" name="last_name">
                    </div>

                    @if ($errors->has('last_name'))
                        <span class="help-block">
                            <strong><h4>{{ $errors->first('last_name') }}</h4></strong>
                        </span>
                    @endif
                </div>
            </div>
            
            <!-- Mid name -->
            <div class="form-group{{ $errors->has('mid_name') ? ' has-error' : '' }}">
                <label for="mid_name" class="control-label col-sm-2"><h4>Отчество</h4></label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="mid_name">
                </div>
                @if ($errors->has('mid_name'))
                    <span class="help-block">
                        <strong><h4>{{ $errors->first('mid_name') }}</h4></strong>
                    </span>
                @endif
            </div>
            
            <!-- Last name -->
            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                <label for="first_name" class="control-label col-sm-2"><h4>Имя</h4></label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="first_name">
                </div>
                @if ($errors->has('first_name'))
                    <span class="help-block">
                        <strong><h4>{{ $errors->first('first_name') }}</h4></strong>
                    </span>
                @endif
            </div>
            
            <!-- Age -->
            <div class="form-group{{ $errors->has('age') ? ' has-error' : '' }}">
                <label class="control-label col-sm-2" for="age"><h4>Возраст</h4></label>
                <div class="col-sm-3">
                    <input class="form-control" type="number" name="age" min="18" max="200">
                </div>
                @if ($errors->has('age'))
                    <span class="help-block">
                        <strong><h4>{{ $errors->first('age') }}</h4></strong>
                    </span>
                @endif
            </div>
            
            <!-- Gender -->
            <div class="form-group">
                <label class="control-label col-sm-2" for="gender">Пол</label>
                <div class="col-sm-3">
                    <select name="gender" id="gender" class="form-control">
                        <option value="M">Муж.</option>
                        <option value="F">Жен.</option>
                    </select>
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-sm-2 col-sm-offset-5">
                    <input class="btn btn-primary" type="submit" value="Сохранить">
                </div>
            </div>

        </form>
        <!-- End of form -->
    </div>
</div>
<!-- End of panel form change profile -->

@endsection
