@extends('layouts.app')

@section('content')
<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <!-- Product's images -->
            <div class="col-md-3">
                <div class="carousel-box">
                    <div id="carousel-content" class="carousel slide" data-ride="carousel" data-interval="false" data-wrap="false">
                        
                        <!-- Slides -->
                        <div class="carousel-inner" role="listbox">
                            
                            <div class="item active">
                                <img class="carousel-picture" src="/uploads/product_images/{{ $auction->product->photos[0]->filename }}">
                            </div>

                            @foreach ($auction->product->photos as $photo)
                                @if ($loop->index > 0)
                                <div class="item">
                                    <img class="carousel-picture" src="/uploads/product_images/{{ $photo->filename }}">
                                </div>
                                @endif
                            @endforeach
                        </div>
                        
                        @if ($auction->product->photos->count() > 1)
                        <!-- Control -->
                        <a class="left carousel-control" href="#carousel-content" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-content" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                        @endif
                    </div>
                </div>
            </div>
            <!-- End of product's image -->

            <div class="col-md-3">
                <h3>{{ $auction->product->name }}</h3>

                <p>Текущая цена: <span id="price">{{ $auction->bids->max('value') ? $auction->bids->max('value') : $auction->price }}</span> монет</p>
                
                <form id="bid-form">
                    <input class="btn btn-primary" type="submit" value="Сделать ставку">            
                    <input name="user_id" type="hidden" value="{{ Auth::user()->id }}">
                    <input name="auction_id" type="hidden" value="{{ $auction->id }}">

                    <input class="form-control" id="bid-value" type="number" name="value" >
                    <span>м.</span>
                    <div id="error"></div>
                </form>

                <button id="buy" value="{{ $auction->max_price }}" class="btn btn-primary">Выкупить товар: {{ $auction->max_price }} монет</button>
            </div>

            <div class="col-md-3">
                <p>Продавец: <strong id="owner">{{ $auction->product->user->username }}</strong></p>
                <p>Начало торгов: {{ date("Y-m-d H:i", $auction->start_at) }}</p>
                <p>Окончание торгов: {{ date("Y-m-d H:i", $auction->end_at) }}</p>
                <p>Начальная цена: {{ $auction->price }} монет</p>
                <p>Максимальная цена: {{ $auction->max_price }} монет</p>
            </div>

            <div class="col-md-3">
                <span style="display: none;" id="cowndown">{{ $auction->end_at }}</span>
        
                <div id="div-clock">
                    <span>До окончания торгов осталось: </span>
                    <p>
                        <span class="clock-number" id="days"></span> дн.
                        <span class="clock-number" id="hours"></span> час.
                        <span class="clock-number" id="minutes"></span> мин.
                        <span class="clock-number" id="seconds"></span> сек.
                    </p>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-9">
                <h3>Описание товара</h3>
                <p>{{ $auction->product->description }}</p>        
            </div>

            <div class="col-md-3">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Логин</th>
                            <th>Последняя цена</th>
                            <th>Время</th>
                        </tr>
                    </thead>

                    <tbody id="history-table">
                        @foreach ($auction->bids as $bid)
                        <tr>
                            <th>{{ $bid->user->username }}</th>
                            <td>{{ $bid->value }}</td>
                            <td>{{ $bid->created_at }}</td>
                        </tr>    
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>                
</div>

<!-- Modal for end auction -->
<div role="dialog" class="modal fade" id="modal-auction-ended" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Торги завершены</h4>
            </div>
            
            <div class="modal-footer">
                <p id="auction-end-info"></p>
                <a href="/home">Вернуться на главную страницу</a>
            </div>
        </div>
    </div>
</div>
<!-- End of modal for reply message -->

<!-- Modal for log out  -->
<div role="dialog" class="modal fade" id="modal-log-out" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Log out</h4>
            </div>
            
            <div class="modal-footer">
                <p id="auction-log-out">
                    Your account is lock by admin!
                </p>
                <a href="{{ url('/logout') }}"
                    onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                    Log out
                </a>
            </div>
        </div>
    </div>
</div>
<!-- End of modal for reply message -->

@endsection

@push('styles')
    <link href="/css/style.css" rel="stylesheet">
@endpush

@push('scripts')
    <script src="/js/auction.js"></script>
@endpush
