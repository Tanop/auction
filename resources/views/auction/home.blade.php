@extends('layouts.app')

@section('content_header')
<div class="jumbotron">
  <h2><span id="greeting"></span> {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</h2>
  <p>Текущее время: <span id="clock"></span></p>
</div>
@endsection

@section('content')
<div class="row">
@foreach ($auctions as $auction)
  <div class="col-md-3 text-center">
    <div class="single-auction-form @if (!$auction->active) inactive-auction @endif">
      <div class="media">
        <div class="media-left">
        <a @if ($auction->public || $auction->users->contains(Auth::user())) href="/auction/{{ $auction->id }}" @endif>
          <img style="max-width: 150px; height: auto;" src="/uploads/product_images/{{ $auction->product->photos[0]->filename }}" alt="Product's Image">
        </a>
        </div>
        <div class="media-body">
          <h4>{{ $auction->product->name }}</h4>
          <p>{{ $auction->product->description }}</p>
        </div>
      </div>

        <div class="auction-detail text-center">

            <div class = "description">
                <p>{{ $auction->product->description }}</p>
            </div>

            <div class ="lot-parametrs">
                <p>Текущая цена: {{ $auction->bids->max('value') ? $auction->bids->max('value') : $auction->product->price }} монет</p>
                <p>Дата окончания торгов: {{ date("Y.m.d H:i", $auction->end_at) }}</p>
                <p>Продавец: {{ $auction->product->user->username }}</p>
            </div>

        @if (!$auction->public && !$auction->users->contains(Auth::user()) )
          <a href="#" id="send-request-{{ $auction->id }}" value="{{$auction->id}}" data-to-id="{{$auction->owner_id}}" class="btn-send-request btn btn-primary">Send request</a>
          <a href="/auction/{{ $auction->id }}" class="disabled btn btn-primary">Вступить в торги</a>
        @else 
          <a href="/auction/{{ $auction->id }}" class="btn btn-primary">Вступить в торги</a>          
        @endif
        
      </div>
    </div>
  </div>
@endforeach
</div>
@endsection

@push('styles')
  <link href="/css/style.css" rel="stylesheet">
@endpush

@push('scripts')
  <script src="/js/home.js"></script>
@endpush