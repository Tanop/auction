@extends('layouts.app')

@section('content')
<!-- Table of all messages, which is sent to this user -->
<div class="panel panel-default">
    <div class="panel-heading">
        <h4>Уведомления</h4>
    </div>    

    <div class="panel-body">
        <table class="table">
        	<thead>
        		<th>Сообщение</th>
        		<th width="25%"></th>
        	</thead>
        	<tbody>
        		@foreach ($notifications as $notification)
        		<tr id="ms-row-{{ $notification->id }}">        			
        			<td>{{ $notification->message }}</td>
        			<td>
        				<button class="delete btn btn-danger" value="{{ $notification->id }}">Удалить</button>
        			</td>
        		</tr>
        		@endforeach
        	</tbody>
        </table>
    </div>
</div>
<!-- End of table -->

<!-- Link to back page -->
<div class="panel panel-default">
    <div class="panel-body">
        <a href="/product" class="btn btn-primary">В личный кабинет</a>
    </div>
</div>
<!-- End of link to back page -->

@endsection

@push('scripts')
	<script src="/js/notification.js"></script>
@endpush