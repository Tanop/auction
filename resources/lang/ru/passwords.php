<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен состоять минимум из 6 символов.',
    'reset' => 'Ваш пароль был сброшен!',
    'sent' => 'Мы отправили вам на почту информацию о сбросе пароля!',
    'token' => 'Неверный токен сброса пароля.',
    'user' => "Не удалось найти пользователя с таким email адресом.",

];
