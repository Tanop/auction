<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Атрибут :attribute должен быть принят.',
    'active_url'           => 'Атрибут :attribute не является корректным URL.',
    'after'                => 'Атрибут :attribute должен быть датой позже, чем :date.',
    'alpha'                => 'Атрибут :attribute может содердать только буквы.',
    'alpha_dash'           => 'Атрибут :attribute может содержать только буквы, числа и тире.',
    'alpha_num'            => 'Атрибут :attribute может содержать только буквы и числа.',
    'array'                => 'Атрибут :attribute должен быть массивом.',
    'before'               => 'Атрибут :attribute должен быть датой раньше, чем :date.',
    'between'              => [
        'numeric' => 'Число :attribute должен быть больше :min и меньше :max.',
        'file'    => 'Файл :attribute должен иметь объем больше :min и меньще :max килоюайт.',
        'string'  => 'Строка :attribute должнасодержать больще :min и меньше :max символов.',
        'array'   => 'Массив :attribute должен быть иметь больше :min и меньше :max элементов.',
    ],
    'boolean'              => 'Атрибут :attribute должен принимать значения истина или ложь.',
    'confirmed'            => 'Атрибут :attribute и его подтверждение не соответствуют.',
    'date'                 => 'Атрибут :attribute не является датой.',
    'date_format'          => 'Атрибут :attribute не соответствует формату :format.',
    'different'            => 'Атрибут :attribute и :other должны отличаться.',
    'digits'               => 'Атрибут :attribute должен состоять из :digits цифр.',
    'digits_between'       => 'Атрибут :attribute должен состоять больше, чем из :min и меньше, чем из :max цифр.',
    'dimensions'           => 'Атрибут :attribute имеет неверный размер изображения.',
    'distinct'             => 'Атрибут :attribute имеет повторяющееся значение.',
    'email'                => 'Атрибут :attribute должен быть корректным email адресом.',
    'exists'               => 'Выбранный атрибут :attribute неверен.',
    'file'                 => 'Атрибут :attribute должен быть файлом.',
    'filled'               => 'Требуется атрибут :attribute.',
    'image'                => 'Атрибут :attribute должен быть изображением.',
    'in'                   => 'Выбранный атрибут :attribute неверен.',
    'in_array'             => 'Атрибут :attribute не существует в :other.',
    'integer'              => 'Атрибут :attribute должен быть целочисленным.',
    'ip'                   => 'Атрибут :attribute должен быть корректным IP адресом.',
    'json'                 => 'Атрибут :attribute должен быть корректной JSON строкой.',
    'max'                  => [
        'numeric' => 'Число :attribute не может быть больше :max.',
        'file'    => 'Файл :attribute не можеть иметь объем больше :max килобайт.',
        'string'  => 'Строка :attribute не может состоять более, чем из :max символов.',
        'array'   => 'Массив :attribute не может иметь больше :max элементов.',
    ],
    'mimes'                => 'Атрибут :attribute должен быть файлом типа: :values.',
    'mimetypes'            => 'Атрибут :attribute должен быть файлом типа: :values.',
    'min'                  => [
        'numeric' => 'Чисдлв :attribute не должно быть меньше :min.',
        'file'    => 'Файл :attribute не должен иметь объем меньше :min килобайт.',
        'string'  => 'Строка :attribute не должна быть меньше :min символов.',
        'array'   => 'Массив :attribute не должен иметь меньше :min элементов.',
    ],
    'not_in'               => 'Выбранный атрибут :attribute неверен.',
    'numeric'              => 'Атрибут :attribute должен быть числом.',
    'present'              => 'Атрибут :attribute должен существовать.',
    'regex'                => 'Формат атрибута :attribute некорректен.',
    'required'             => 'Необходим атрибут :attribute.',
    'required_if'          => 'Атрибут :attribute требуется, когда :other = :value.',
    'required_unless'      => 'Атрибут :attribute требуется, пока :other в :values.',
    'required_with'        => 'Атрибут :attribute трубеутся, когда значение :values представлено.',
    'required_with_all'    => 'Атрибут :attribute требуется, когда значение :values представлено.',
    'required_without'     => 'Атрибут :attribute требуется, когда значение :values не представлено.',
    'required_without_all' => 'Атрибут :attribute требуется, когда ни одно из значений :values не представлено.',
    'same'                 => 'Атрибут :attribute и :other должны совпадать.',
    'size'                 => [
        'numeric' => 'Число :attribute быть равно :size.',
        'file'    => 'Файл :attribute должен иметь объем, равный :size килобайт.',
        'string'  => 'Строка :attribute должна состоять из :size символов.',
        'array'   => 'Массив :attribute должен содержать :size элементов.',
    ],
    'string'               => 'Атрибут :attribute должен быть строкой.',
    'timezone'             => 'Атрибут :attribute должен быть корректным часовым поясом.',
    'unique'               => 'Атрибут :attribute уже занят.',
    'uploaded'             => 'Не удалось загрущить :attribute.',
    'url'                  => 'Формат :attribute некорректен.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
